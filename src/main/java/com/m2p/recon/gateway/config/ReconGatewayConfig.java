package com.m2p.recon.gateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.m2p.recon.gateway.filter.JwtAuthenticationFilter;

@Configuration
public class ReconGatewayConfig {

	@Value("${gateway.fileservice.url}")
	String fileserviceGatewayurl;

	@Value("${gateway.reportservice.url}")
	String reportServiceGatewayurl;

	@Value("${gateway.notification.url}")
	String notificationServiceGatewayurl;
	
	@Value("${gateway.admin.url}")
	String adminServiceGatewayurl;
	

	@Value("${gateway.fileservice.path}")
	String fileserviceGatewayPath;

	@Value("${gateway.reportservice.path}")
	String reportServiceGatewayPath;

	@Value("${gateway.notification.path}")
	String notificationServiceGatewayPath;
	
	@Value("${gateway.admin.path}")
	String adminServiceGatewayPath;
	

	@Autowired
	private JwtAuthenticationFilter filter;

	@Bean
	public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
		return builder.routes()
				.route(r -> r.path(adminServiceGatewayPath).filters(f -> f.filter(filter)).uri(adminServiceGatewayurl).id("adminService"))
				.route(r -> r.path(fileserviceGatewayPath).filters(f -> f.filter(filter)).uri(fileserviceGatewayurl).id("fileservice"))
				.route(r -> r.path(notificationServiceGatewayPath).filters(f -> f.filter(filter)).uri(notificationServiceGatewayurl).id("notification"))
				.route(r -> r.path(reportServiceGatewayPath).filters(f -> f.filter(filter)).uri(reportServiceGatewayurl).id("reportservice"))
				.build();
	}

}